[dsv2sql](https://gitlab.com/onlyjob/dsv2sql/raw/master/dsv2sql) (could be called `csv2sql`)
converts CSV (character-separated values) a.k.a. DSV (Delimiter-separated values)
file(s) to `SQL INSERT` statements.
When multiple files given, rows can be merged on unique ID fields (UID).

Alternatively this utility can print results in CSV format.

`dsv2sql` can be used to merge different CSV files (i.e. files with
different columns) into one file where records are consolidated by unique
key filed.

`dsv2sql` written in Perl 5.10 and uses `DBI` and `Text::CSV` modules.
